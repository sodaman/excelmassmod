use strict;
use warnings;

print "Initializing...\n";

my $program_directory;

BEGIN {
	use Cwd;
	$program_directory = cwd;
}

print " - ", "Importinfilg modules... ";

use lib $program_directory."/Modules",
        $program_directory."/Modules/Report",
		$program_directory."/Modules/Excel_Tunnel";

use Report;
use Excel_Tunnel;

use Run_User_Crit;

print "done.\n"; # Importing modules

# Import Spreadsheet::Read
print " - ", "Importing 'Spreadsheet::Read'... ";
use Spreadsheet::Read;
print "done.\n";

=for File Choices
Harris
Kent South
mission 115
=cut

print " - ", "Setting file variables... ";
my $file_name = "mission 115";
my $file_ext  = "xls";
my $file_path = "C:\\RTscada\\database";
print "done.\n";

print "Initialization complete.\n\n";

# ---> Begin Main
my $DataReport = Excel_Tunnel(
	{
		file_name => $file_name,
		file_ext  => $file_ext,
		file_dir  => $file_path,
	}
);

$DataReport -> print_report ("${file_path}\\${file_name}_report.txt"); #

print "\n";

my $exit_var = 0;

while ($exit_var == 0) {

	my $User_Crit_Hash_Ref = Get_User_Crit();

	my $UserSearchReport   = Run_User_Crit ($User_Crit_Hash_Ref, $DataReport);

	$UserSearchReport -> print_report();

	print "\n",
	      "Run another report? [Y/n] ";
	chomp (my $UserInput = <>);

	if ($UserInput =~ /n/i) {
		$exit_var = 1;
	}

}