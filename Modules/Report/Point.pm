use strict;
use warnings;

package Point;



# Constructor
sub new {
	my $class = shift;

	my $InputDataHashRef = shift;

	my $self = {};
	bless $self, $class;

	$self -> initialize ($InputDataHashRef);

	return $self;
}


# Initialization Engine
sub initialize {
	# Do stuff
	my $self = shift;

	my $InputDataHashRef = shift;

	$self -> {'name'} = exists ($InputDataHashRef -> {'name'}) ? $InputDataHashRef -> {'name'} : '';
	$self -> {'row'}  = exists ($InputDataHashRef -> {'row'})  ? $InputDataHashRef -> {'row'}  : '';
	$self -> {'data'} = exists ($InputDataHashRef -> {'data'}) ? $InputDataHashRef -> {'data'} : [];
	# Point data values.  Actual attribute names are found by accessing the table->fields[same_index as point field]->attribute

	return;
}


# Setter methods
sub set_name {
	my $self = shift;

	my $new_data_value = shift;

	$self -> {'name'} = $new_data_value;

	return;
}

sub set_row {
	my $self = shift;

	my $new_data_value = shift;

	$self -> {'row'} = $new_data_value;

	return;
}

sub set_data {
	my $self = shift;

	my $new_data_ref = shift;

	$self->{'data'} = $new_data_ref;

	return;
}


# Printing a point report
sub print_point_report {
	my $self = shift;

	my $ThisPointName = $self -> {'name'};
	my $ThisPointRow  = $self -> {'row'};
	my $ThisPointData = $self -> {'data'};

	# Start by printing the point row location & name.
	print "$ThisPointName ($ThisPointRow): ";

	for (my $index = 0; $index <= $#{ $ThisPointData }; $index++) {

		print $ThisPointData -> [$index];

		print ", " if $index < $#{ $ThisPointData };

	}

	print "\n";
	
	return;
}



return 1;