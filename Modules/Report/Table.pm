use strict;
use warnings;

package Table;

use Point;

# Constructor
sub new {
	my $class = shift;

	my $self = {};
	bless $self, $class;

	$self -> initialize();

	return $self;
}


# Initialization Engine
sub initialize {
	# Do stuff
	my $self = shift;

	$self -> {'no_points'} = 0;
	$self -> {'fields'} = []; # Usage: $self -> {'fields'} -> [$index] -> { attribute => "Parameter", column => column number (integer) }
	$self -> {'point_list'} = [];

	return;
}


# Setting the 'fields' array
sub set_fields {
	my $self = shift;

	my $new_fields_ref = shift;

	$self -> {'fields'} = $new_fields_ref;

	return;
}


# Adding a point
sub add_point {
	my $self = shift;

	my $new_point_hash_ref = shift; # Contains name, row & data fields.

	my $new_point_name = $new_point_hash_ref -> {'name'};

	push @{ $self->{'point_list'} }, $new_point_name;

	$self -> {$new_point_name} = 'Point' -> new ($new_point_hash_ref);

	return;
}


# Printing a table report
sub print_table_report {
	my $self = shift;

	if ($#{ $self->{'fields'} } >= 0) {

		print "Fields-> ";

		# print " | Printable | Sample: ", $self -> {'fields'} -> [0] -> {'attribute'};

		for (my $index = 0; $index <= $#{ $self->{'fields'} }; $index++) {
			my $ThisAttribute = $self -> {'fields'} -> [$index] -> {'attribute'};
			my $ThisColumn    = $self -> {'fields'} -> [$index] -> {'column'};

			print "$ThisAttribute ($ThisColumn)";

			print ", " if $index < $#{ $self->{'fields'} };

		}

	}

	print "\n";

	if ($#{ $self->{'point_list'} } >= 0) {
		print "<Points>\n";

		for (my $index = 0; $index <= $#{ $self->{'point_list'} }; $index++) {
			
			my $ThisPoint = $self -> {'point_list'} -> [$index];

			$self -> {$ThisPoint} -> print_point_report();

		}
	}

	print "\n";

	return;
}



return 1;