use strict;
use warnings;

package Device;

use Table;

# Constructor
sub new {
	my $class = shift;

	my $self = {};
	bless $self, $class;

	$self -> initialize();

	return $self;
}


# Initialization Engine
sub initialize {
	# Do stuff
	my $self = shift;

	$self -> {'table_list'} = [];

	return;
}


# Adding a table
sub add_table {
	my $self = shift;

	my $new_table_name = shift;

	push @{ $self->{'table_list'} }, $new_table_name;

	$self -> {$new_table_name} = 'Table' -> new();

	return;
}


# Printing a device report
sub print_device_report {
	my $self = shift;

	if ( $#{ $self->{'table_list'} } > -1) {

		foreach (@{ $self->{'table_list'} }) {
			my $ThisTable = $_;

			print " " x 8, "Table: $ThisTable\n";

			$self -> {$ThisTable} -> print_table_report();

		}

	}

	return;
}



return 1;