use strict;
use warnings;

package Page;

use Device;

# Constructor
sub new {
	my $class = shift;

	my $page_label = shift;

	my $self = {};
	bless $self, $class;

	$self -> initialize($page_label);

	return $self;
}


# Initialization Engine
sub initialize {
	# Do stuff
	my $self = shift;

	my $page_label = shift;

	$self -> {'label'} = $page_label;
	$self -> {'device_list'} = [];

	return;
}


# Label reporting feature
sub print_label {
	my $self = shift;

	print "Label: ".$self -> {'label'}."\n";

	return;
}


# Adding a device
sub add_device {
	my $self = shift;

	my $new_device_name = shift;

	push @{ $self->{'device_list'} }, $new_device_name;

	$self -> {$new_device_name} = 'Device' -> new();


}


# Printing a page report
sub print_page_report {
	my $self = shift;

	foreach (@{ $self->{'device_list'} }) {
		my $ThisDevice = $_;

		print " " x 4, "Device: $ThisDevice\n";

		$self -> {$ThisDevice} -> print_device_report();

	}
}



return 1;