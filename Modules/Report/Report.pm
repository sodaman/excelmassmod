use strict;
use warnings;

package Report;

use Page;

# Constructor
sub new {
	my $class = shift;

	my $self = {};
	bless $self, $class;

	$self -> initialize();

	return $self;
}


# Initialization Engine
sub initialize {
	my $self = shift;
	
	$self -> {'sheets'} = []; # Array of sheet *numbers* in Report.  Used for indexing through pages.

	return;
}


=for Adding a page to the report.
Incoming Sheet:
-> sheet_number
-> label
=cut
sub add_sheet {
	my $self = shift;
	my $Incoming_Sheet_And_Label_Hash_Ref = shift;

	my $CurrentSheets = $self -> {'sheets'};

	my $New_Sheet_Number = $Incoming_Sheet_And_Label_Hash_Ref -> {'sheet_number'};
	my $New_Sheet_Label  = $Incoming_Sheet_And_Label_Hash_Ref -> {'label'};

	$self -> {$New_Sheet_Number} = 'Page' -> new ($New_Sheet_Label);
	
	push @{$CurrentSheets}, $Incoming_Sheet_And_Label_Hash_Ref->{'sheet_number'};

	return;
}


# Subroutine for printing meta-data
sub print_sheets {
	my $self = shift;
	print join (" ", @{ $self -> {'sheets'} });
	print "\n";

	return;
}


# Subroutine for printing a dump of the report.
sub print_report {
	my $self = shift;

	my $TargetFile = shift;

	my $old_handle = select;

	select_output_handle ($TargetFile);

	my $CurrentSheets = $self -> {'sheets'};

	# Loop for printing report
	foreach (@{$CurrentSheets}) {

		my $ThisSheet = $_;

		print "Sheet: $ThisSheet\n";
		
		$self -> {$ThisSheet} -> print_label();

		$self -> {$ThisSheet} -> print_page_report();

		print "\n\n";
	}

	# Block for closing the TEST_OUT handle if it was used.
	{
	    no warnings;
	    if (tell (TEST_OUT) != -1) {
	        close (TEST_OUT);
	    }
	}

	select ($old_handle);
	print "Returned to $old_handle!\n";

	return;
}


# Subroutine for selecting report output FH
sub select_output_handle {
    my $TargetFile = shift;

    if (defined $TargetFile) {
        open (TEST_OUT, '>', $TargetFile) or die "Can't open \"$TargetFile\" for reason: $!\n";
        print "Opened file: $TargetFile!\n";
        select (TEST_OUT);
    }

    return;
}


# Inspects a dataspace and will positively create it if it DNE.
sub Make_Data_Space {
	my $self = shift;

	my $InputHash = shift;

	my $TargetSheet  = $InputHash -> {'sheet'};
	my $TargetLabel  = $InputHash -> {'label'};
	my $TargetDevice = $InputHash -> {'device'};
	my $TargetTable  = $InputHash -> {'table'};

	if (!exists $self -> {$TargetSheet}) {
		# print "Adding sheet... ";
		$self -> add_sheet({ sheet_number => $TargetSheet,
			                 label        => $TargetLabel  });
	}

	if (!exists $self -> {$TargetSheet} -> {$TargetDevice}) {
		# print "Adding device... ";
		$self -> {$TargetSheet} -> add_device ($TargetDevice);
	}

	if (!exists $self -> {$TargetSheet} -> {$TargetDevice} -> {$TargetTable}) {
		# print "Adding table... ";
		$self -> {$TargetSheet} -> {$TargetDevice} -> add_table ($TargetTable);
	}

	# else { print "Data space exists!\n"; }
}


# Checks for a dataspace and will negatively return if it DNE.
sub Check_Data_Space {
	my $self = shift;

	my $InputHash = shift;

	my $TargetSheet  = $InputHash -> {'sheet'};
	my $TargetLabel  = $InputHash -> {'label'};
	my $TargetDevice = $InputHash -> {'device'};
	my $TargetTable  = $InputHash -> {'table'};

	# Check if sheet exists in the report.
	if (exists $self -> {$TargetSheet}) {

		# Check if device exists in the sheet.
		if (exists $self -> {$TargetSheet} -> {$TargetDevice}) {

			# Check if table exists in the device.
			if (exists $self -> {$TargetSheet} -> {$TargetDevice} -> {$TargetTable}) {

				return 1; # Data space exists!
	
			}
			
		}
		
	}

	else { return 0; } # Data spce DNE!

}


#<eof>
return 1;