use strict;
use warnings;

=for User Search Report

This file stores subroutines used for getting user search criteria & running the search over the report.

=cut


# Subroutine for grabbing user search criteria.
sub Get_User_Crit {
	my $UserQueryCrit = {};
	my $UserInput;

	# print "\n",
	#       "Need help? [y/N] ";
	# chomp ($UserInput = <>);

	# if ($UserInput =~ /y/i) {
	# 	Get_User_Crit_Help();
	# }

	print "Enter search criteria:\n";
	printf("%12s", "Label: ");
	chomp($UserInput = <>);
	$UserInput = rtrim($UserInput);
	$UserQueryCrit -> {label} = $UserInput;

	printf("%12s", "Device: ");
	chomp($UserInput = <>);
	$UserInput = rtrim($UserInput);
	$UserQueryCrit -> {device} = $UserInput;

	printf("%12s", "Table: ");
	chomp($UserInput = <>);
	$UserInput = rtrim($UserInput);
	$UserQueryCrit -> {table} = $UserInput;

	printf("%12s", "Point: ");
	chomp($UserInput = <>);
	$UserInput = rtrim($UserInput);
	$UserQueryCrit -> {point} = $UserInput;

	printf("%12s", "Attribute: ");
	chomp($UserInput = <>);
	$UserInput = rtrim($UserInput);
	$UserQueryCrit -> {attribute} = [split (" ", $UserInput)];

	print "-" x 12, "\n";

	# Print_User_Crit ($UserQueryCrit);

    return $UserQueryCrit;

}


# User Search Criteria Help Function
sub Get_User_Crit_Help {
	print "User search criteria structuring:\n",
	      "Enter a list of space-delimited search items.\n",
	      "This forms a 'filter'.  Multiple search filters\n",
	      "can be specified by separating them with commas.\n",
	      "ie, 'phase current, power' will match where:\n",
	      " - Target has matches for 'phase' AND 'current'\n",
	      " - OR Target has match for 'power'\n";

	      return 1;
}


# Subroutine for printing user search criteria back to the screen (debugging messages)
sub Print_User_Crit {
	my $UserQueryCrit = shift;

	print "Captured input report:\n";

	print "Label:  ";
	if ($UserQueryCrit -> {label} eq '') {
		print "Not specified.";
	}
	else{ print $UserQueryCrit -> {label}; }

	print "\n";

	print "Device: ";
	if ($UserQueryCrit -> {device} eq '') {
		print "Not specified.";
	}
	else{ print $UserQueryCrit -> {device}; }

	print "\n";

	print "Table:  ";
	if ($UserQueryCrit -> {table} eq '') {
		print "Not specified.";
	}
	else{ print $UserQueryCrit -> {table}; }

	print "\n";

	print "Point:  ";
	if ($UserQueryCrit -> {point} eq '') {
		print "Not specified.";
	}
	else{ print $UserQueryCrit -> {point}; }

	print "\n";

	print "Attribute: ";
	if ($#{$UserQueryCrit -> {attribute}} == -1) {
		print "None specified.";
	}
	else{ print join ", ", @{ $UserQueryCrit -> {attribute} }; }

	print "\n";

	return;
}


# Subroutine for running the user search criteria.
sub Run_User_Crit {
	my $User_Crit_Hash_Ref = shift;

	my ($User_Label, $User_Device, $User_Table, $User_Point, $User_Attribute) = (
		$User_Crit_Hash_Ref -> {label},
		$User_Crit_Hash_Ref -> {device},
		$User_Crit_Hash_Ref -> {table},
		$User_Crit_Hash_Ref -> {point},
		$User_Crit_Hash_Ref -> {attribute}
	);

	my $TargetReport = shift;

	my $UserSearchReport = 'Report' -> new();

	SHEET: foreach ( @{$TargetReport -> {'sheets'}} ) {

		my $Current_Sheet = $_;

		if ($User_Crit_Hash_Ref -> {label} ne '') {
			next SHEET if ($TargetReport -> {$Current_Sheet} -> {'label'} !~ /$User_Label/i);
		}

		DEVICE: foreach ( @{$TargetReport -> {$Current_Sheet} -> {'device_list'}} ) {

			my $Current_Device = $_;

			if ($User_Crit_Hash_Ref -> {device} ne '') {
				next DEVICE if ($Current_Device !~ /$User_Device/i);
			}

			TABLE: foreach ( @{$TargetReport -> {$Current_Sheet} -> {$Current_Device} -> {'table_list'}} ) {

				my $Current_Table = $_;

				if ($User_Crit_Hash_Ref -> {table} ne '') {
					next TABLE if ($Current_Table !~ /$User_Table/i);
				}

				my $Target_Table_Anchor = $TargetReport->{$Current_Sheet}->{$Current_Device}->{$Current_Table};

				my $Fields  = [];
				my $Columns = [];
				my $Points  = [];

				# Check table attributes (if applicable)
				if ( $#{$User_Crit_Hash_Ref -> {attribute}} >= 0 ) {
					# Found user attributes.  Iterate over table attributes.

					ATTR: for (my $i = 0; $i <= $#{ $Target_Table_Anchor->{'fields'} }; $i++ ) {

						foreach (@{$User_Attribute}) {

							if ( $Target_Table_Anchor->{'fields'}->[$i]->{'attribute'} =~ /$_/i ) {
								push @{$Fields}, $Target_Table_Anchor->{'fields'}->[$i];
								push @{$Columns}, $i;
								next ATTR;
							}

						}

					} # End of ATTR block.
				}

				else {
					# Grab all table columns.
					$Fields = $Target_Table_Anchor->{'fields'};
					$Columns = [( 0..$#{$Target_Table_Anchor->{'fields'}} )];
				}

				next TABLE if ($#{$Fields} == -1);

				# Check table points list (if attributes found)
				if ($User_Point ne '') {
					POINT: for (my $j = 0; $j <= $#{$Target_Table_Anchor->{'point_list'}}; $j++) {
						my $Current_Point = $Target_Table_Anchor->{'point_list'}->[$j];

						if ($User_Point ne '') {
							next POINT if ($Current_Point !~ /$User_Point/i);
						}

						push @{$Points}, $Current_Point;

					} # End of POINT block.

				}

				else {
					# Grab all table points.
					$Points = $Target_Table_Anchor->{'point_list'};
				}

				next TABLE if ($#{$Points} == -1);

                $UserSearchReport -> Make_Data_Space ({ sheet  => $Current_Sheet,
                                                        label  => $TargetReport -> {$Current_Sheet} -> {'label'},
                                                        device => $Current_Device,
                                                        table  => $Current_Table });

                $UserSearchReport -> {$Current_Sheet} -> {$Current_Device} -> {$Current_Table} -> set_fields ($Fields);

                foreach (@{$Points}) {
                	$UserSearchReport -> {$Current_Sheet} -> {$Current_Device} -> {$Current_Table} -> add_point ({
                		name => $_,
                		row  => $Target_Table_Anchor->{$_}->{'row'},
                		data => [ @{ $Target_Table_Anchor->{$_}->{'data'} } [ @{$Columns} ] ]
                	});
                }				

			} # End of TABLE block.

		} # End of DEVICE block.

	} # End of SHEET block.

	return $UserSearchReport;
	
}


return 1;