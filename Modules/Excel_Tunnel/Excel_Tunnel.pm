use strict;
use warnings;

use Spreadsheet::Read;
use StateMachineFunctions;

# Excel Tunnel subroutine (hides state machine and returns a filled report) [WARNING: CALLER MUST USE REPORT!]
sub Excel_Tunnel {
	my $InputHash = shift;

	my $FileHandle = $InputHash->{'file_dir'}."\\".
	                 $InputHash->{'file_name'}.".".$InputHash->{'file_ext'};

	print "Built FH: $FileHandle\n";

	my $book = ReadData ($FileHandle, parser => $InputHash->{'file_ext'});

	print "File read complete!\n";

	my $NewReport = 'Report' -> new(); # Initialize new Report.  Will fill, then return to caller.

	# State Machine Variables
	my $device;
	my $table;
	my $LastColumnVerb;

	# Coordinate navigation variables.
	my $SheetNumber = 1;
	my $row = 1;
	my $col = 1;

	my $CellCoords = {
		book  => $book,
		sheet => \$SheetNumber,
		row   => \$row,
		col   => \$col
	};

	my $noSheets = $book->[0]{sheets};

	# ---> Begin Main

	for ($SheetNumber = 2; $SheetNumber <= $noSheets; $SheetNumber++) {

		# State control and workflow variables.
		my $state = 'FindFirst';
		my $YouMayProceed = 0;
		my $NextObject = '';

		my $Input_Data_Ref  = [];
		my $Output_Data_Ref = [];

		$NewReport -> add_sheet(
			{
				sheet_number => $SheetNumber,
				label        => $book -> [$SheetNumber] {label}
			}
		);

		ResetSheetVariables($CellCoords, \$YouMayProceed);

	    # Enter the state-machine hub/controller
		STATE_CYCLE: do {

			$Output_Data_Ref = Unpacker($CellCoords, $state, $Input_Data_Ref);

	        # Find a verb within the 1st 35 rows
			if ($state eq 'FindFirst') {

				$row           = $Output_Data_Ref -> [0];
				$YouMayProceed = $Output_Data_Ref -> [1];

				if ($YouMayProceed == 1) {
					$state = 'FindDevice';
				}

				else { $state = 'Done'; }

			}


	        # Find a device after finding the first verb.
			elsif ($state eq 'FindDevice') {

				$device = $Output_Data_Ref -> [0];

				if ($device eq '') {
				    $state = 'Done'; # Routine dropped out bottom.  No declaration found.
				}
				else { $state = 'FindTable'; }

			}


	        # After finding a device, search for a table.
			elsif ($state eq 'FindTable') {

				$table = $Output_Data_Ref -> [0];

	            if ($table eq '') {
	            	$state = 'Done'; # Routine dropped out bottom.  No declaration found.
	            }
	            else { $state = 'FindNextTable'; }

			}


			# After finding a table, find the next table (or the end of the spreadsheet) to determine the data range.
			elsif ($state eq 'FindNextTable') {

				$LastColumnVerb = $Output_Data_Ref -> [0];
				$NextObject     = $Output_Data_Ref -> [1];

				$state = 'Backfill'; # No matter what, launch the BackFill routine.

				$Input_Data_Ref = [$NewReport, $LastColumnVerb, $device, $table]; # Prepping the Input Data for the Backfill run.

			}


	        # After identifying the end of a table, backfill datapoints.
			elsif ($state eq 'Backfill') {

				if ($NextObject eq '') {
					$state = 'Done';
				}

				if ($NextObject eq 'Table') {
					$state = 'FindTable';
				}

				if ($NextObject eq 'Device') {
					$state = 'FindDevice';
				}

			}

			# $state = 'Done';

		} while ($state ne 'Done');

		# print "Sheet $SheetNumber: Done.\n";
	}

	return $NewReport;

}



return 1;