use strict;
use warnings;

# package StateMachineFunctions;

=for Subroutines Storage File

This file stores subroutines used for reading the Excel file into a Perl datastructure.

=cut

# Subroutine Variables
my ($book, $sheet, $rowNum, $colNum, $Input_Data_Ref, $Output_Data_Ref);

# Excel navigation & searching thresholds.
my $Len_Thresh = 1; # Minimum cell length search threshold
my $firstVerbThresh = 35; # number of rows to look for the 1st verb
my $noEmptyRows = 5; # number of empty rows to look for after the last verb
my $noEmptyCols = 1; # number of empty columns before continuing to the next row

my $sinceLastEmpty = 0;

my $Dispatch_Table = {
	'FindFirst'     => sub { return FindFirst     (@_); },
	'FindDevice'    => sub { return FindDevice    (@_); },
	'FindTable'     => sub { return FindTable     (@_); },
	'FindNextTable' => sub { return FindNextTable (@_); },
	'Backfill'      => sub { return Backfill      (@_); },
	'Done'          => sub { return 1; }
};

### Subroutines ###

sub rtrim {
	my $s = shift;
	$s =~ s/\s+$//;
	return $s
};

sub ltrim {
	my $s = shift;
	$s =~ s/^\s+//;
	return $s;
}


# Subroutine for resetting sheet variables and coordinates.
sub ResetSheetVariables {

	my $CoordsRef = $_[0];
	my $YouMayPassRef = $_[1];

	$$YouMayPassRef = 0;

	${ $CoordsRef -> {'row'} } = 1;
	${ $CoordsRef -> {'col'} } = 1;

};


# Unpacker
sub Unpacker {
	# use vars qw($book $sheet $rowNum $colNum $Input_Data_Ref $Output_Data_Ref);

	# our ($book, $sheet, $rowNum, $colNum, $Input_Data_Ref, $Output_Data_Ref);

	my $CellCoordsRef = shift;

	my $state = shift;

	$Input_Data_Ref = shift;
	$Input_Data_Ref = defined ($Input_Data_Ref) ? $Input_Data_Ref : [];

	$book   = $CellCoordsRef -> {'book'};
	$sheet  = $CellCoordsRef -> {'sheet'};
	$rowNum = $CellCoordsRef -> {'row'};
	$colNum = $CellCoordsRef -> {'col'};

	$Output_Data_Ref = [];

	if (defined ( $Dispatch_Table -> {$state} )) {
		$Output_Data_Ref = $Dispatch_Table -> {$state} -> ($Input_Data_Ref);
	}
	else { die "$state is not a valid dispatch input!\n"; }

	return $Output_Data_Ref;
}


# Subroutine for finding the first verb on a new sheet.
sub FindFirst {

	# print "Sheet $$sheet: Looking for first verb...\n"; # Stage 1 Confirmation -- looking for verbs.

	for ($$rowNum = 1; $$rowNum <= $firstVerbThresh; $$rowNum++){

		if ( Check_Cell_Length ($book -> [$$sheet] {cell} [$$colNum] [$$rowNum], $Len_Thresh) ) {

            # print "Sheet $SheetNumber: Found first verb at row $$rowNum.\n"; # Stage 2 confirmation -- found verbs.
			$sinceLastEmpty = 0;

			$Output_Data_Ref = [$$rowNum, 1];
			return $Output_Data_Ref;

		}
	}
	$Output_Data_Ref = [$$rowNum, 0];
	return $Output_Data_Ref;
}


# Subroutine for finding devices.
sub FindDevice {

	# print "Sheet $$sheet:     Looking for a device from row $$rowNum...\n"; # Stage 3 confirmation -- looking for devices.

	my $newDevice;

	$sinceLastEmpty = 0;

	do{

		# Step 1: Check Length of target cell
		if ( length( $book -> [$$sheet] {cell} [$$colNum] [$$rowNum] ) > $Len_Thresh) {
			$sinceLastEmpty = 0; # Cell is not empty.  Reset sinceLastEmpty.

			# Check cell contents
			# Found table directive?
			if ( lc($book -> [$$sheet] {cell} [$$colNum] [$$rowNum]) eq "table" ){

				# Found device directive?
				if ( lc($book -> [$$sheet] {cell} [$$colNum + 1] [$$rowNum]) =~ /device/ ) {
					$newDevice = $book -> [$$sheet] {cell} [$$colNum + 1] [$$rowNum + 2]; # Have device.  Start looking for tables.
					# print "Sheet $$sheet:     Found device: $newDevice at row $$rowNum.\n"; # Stage 4 confirmation -- found devices.
					
					$Output_Data_Ref = [$newDevice];
					return $Output_Data_Ref;
				}
			}
		}
		else { $sinceLastEmpty += 1; }

		$$rowNum += 1;

	} while ($sinceLastEmpty <= $noEmptyRows);

	$Output_Data_Ref = [''];
	return $Output_Data_Ref;
}


# Subroutine for finding tables
sub FindTable {

	# print "Sheet $$sheet:         Looking for a table starting from row $$rowNum...\n"; # Stage 5 confirmation -- looking for tables.

	my $newTable;

	$sinceLastEmpty = 0;

	do {

		# Step 1: Check Length of target cell
		if ( Check_Cell_Length ($book -> [$$sheet] {cell} [$$colNum] [$$rowNum], $Len_Thresh) ) {
			$sinceLastEmpty = 0; # Cell is not empty.  Reset sinceLastEmpty.

			# Check cell contents
			# Found table directive?
			if ( lc($book -> [$$sheet] {cell} [$$colNum] [$$rowNum] ) eq "table" ) {

				# Found point directive?
				if ( lc($book -> [$$sheet] {cell} [$$colNum + 1] [$$rowNum]) =~ /point/ ) {
					$newTable = $book -> [$$sheet] {cell} [$$colNum + 1] [$$rowNum]; # Have table.  Start looking for values.
					# print "Sheet $$sheet:         Found table: $newTable at row $$rowNum.\n"; # Stage 6 confirmation -- found tables.
					$$rowNum += 1;
					$Output_Data_Ref = [$newTable];
					return $Output_Data_Ref;
				}
			}
		}
		else { $sinceLastEmpty += 1; }

		$$rowNum += 1;

	} while ($sinceLastEmpty <= $noEmptyRows);

	$Output_Data_Ref = [''];
	return $Output_Data_Ref;
}


# Subroutine for finding the next table (and the end of the current data range.)
sub FindNextTable {

	my $NextObject = '';

	# print "Sheet $$sheet:         Looking for next table from row $$rowNum...\n"; # Stage 7 confirmation -- looking for new table or end of file.

	my $lastEncounteredColumnVerb=0;

	$sinceLastEmpty = 0;

	do {

		# Step 1: Check Length of target cell
		if ( Check_Cell_Length ($book -> [$$sheet] {cell} [$$colNum] [$$rowNum], $Len_Thresh) ) {
			$sinceLastEmpty = 0; # Cell is not empty.  Reset sinceLastEmpty.

			# Check cell contents
			# Found column directive?
			if ( lc($book -> [$$sheet] {cell} [$$colNum] [$$rowNum] ) eq "column" ) {
				$lastEncounteredColumnVerb = $$rowNum;
			}

			if ( lc($book -> [$$sheet] {cell} [$$colNum] [$$rowNum] ) eq "table" ) {
				# Hit new table directive.  Roll back to last column verb and backfill datapoints.
				# Will need logic to discern between another table in the same device vs. a new device entirely.

				# print "Sheet $$sheet:         Found next table at row $$rowNum.\n".
				#       "Sheet $$sheet:         Last column verb was at $lastEncounteredColumnVerb.\n";


				# print "Examining cell at row $$rowNum: contains ".$book -> [$$sheet] {cell} [$$colNum + 1] [$$rowNum]."\n";

				if ( lc ( $book -> [$$sheet] {cell} [$$colNum + 1] [$$rowNum] ) =~ /point/ ) {
					$NextObject = 'Table';
					# print "Setting State:NextObject to $State:$NextObject.\n";
				}
				else {
					$NextObject = 'Device';
					# print "Setting State:NextObject to $State:$NextObject.\n";
				}

				$Output_Data_Ref = [$lastEncounteredColumnVerb, $NextObject];
				return $Output_Data_Ref;
				
			}

		}
		else { $sinceLastEmpty += 1; }

		$$rowNum += 1;

	} while ( $sinceLastEmpty <= $noEmptyRows); # && $State eq 'FindNextTable');

	$$rowNum -= 1;

	# print "Sheet $$sheet:         Ran over edge of spreadsheet by row $$rowNum.\n".
	#       "Sheet $$sheet:         Last column verb was at $lastEncounteredColumnVerb.\n";

	# Need logic to launch Backfill if run-over occurs.
	$Output_Data_Ref = [$lastEncounteredColumnVerb, ''];
	return $Output_Data_Ref;

}


# Subroutine for backfilling data.
sub Backfill {

	my $TargetReport = $Input_Data_Ref -> [0];

	my $LastKnownColumnVerb = $Input_Data_Ref -> [1];

	my $NextDevice = $Input_Data_Ref -> [2];
	my $NextTable  = $Input_Data_Ref -> [3];

	# print "Launching Backfill w/ state:object as $State:$NextObject\n";

	# Make dataspace for current target. (Will create dataspace)
	$TargetReport -> Make_Data_Space ({ sheet  => $$sheet,
                                        label  => $book -> [$$sheet] {label},
                                        device => $NextDevice,
                                        table  => $NextTable });

	# Start by grabbing columns data.
	my $Columns = [];

	# print "Grabbing column info from row $LastKnownColumnVerb...\n";

	$sinceLastEmpty = 0;
	my $colIterator = 2;

	do {
		my $column_location = $$colNum + $colIterator;

		if ( Check_Cell_Length ($book -> [$$sheet] {cell} [$column_location] [$LastKnownColumnVerb], $Len_Thresh) ) {
			$sinceLastEmpty = 0; # Cell is not empty.  Reset sinceLastEmpty.

			# print "Pushing ".$book -> [$$sheet] {cell} [$$colNum + $colIterator] [$LastKnownColumnVerb]."\n";
			push @{$Columns}, { attribute => $book -> [$$sheet] {cell} [$$colNum + $colIterator] [$LastKnownColumnVerb],
			                    column    => $column_location };
		}
		else { $sinceLastEmpty += 1; }

		$colIterator += 1;

	} while ($sinceLastEmpty < $noEmptyCols);

	# print join(':', @Columns);
	# print "\n";

	$TargetReport -> {$$sheet} -> {$NextDevice} -> {$NextTable} -> set_fields($Columns);

	BACKFILL_ROW: for (my $rowIterator = $LastKnownColumnVerb+1; $rowIterator <= $$rowNum; $rowIterator++){

		next BACKFILL_ROW if !defined $book -> [$$sheet] {cell} [$$colNum] [$rowIterator];
		
		if ( $book -> [$$sheet] {cell} [$$colNum] [$rowIterator] =~ /value/i ){
			# print "Got new data!\n";
			my $NewData;
			$Columns = [];

			for (my $i = 2; $i < $colIterator-1; $i++){
				# print "Pushing $i:".$book -> [$$sheet] {cell} [$$colNum + $i] [$rowIterator]."\n";
				$NewData = defined ($book -> [$$sheet] {cell} [$$colNum + $i] [$rowIterator]) ? $book -> [$$sheet] {cell} [$$colNum + $i] [$rowIterator] : "";
	            push @{$Columns}, $NewData;
			}

			$TargetReport -> {$$sheet} -> {$NextDevice} -> {$NextTable} -> add_point(
				{
					name => $book -> [$$sheet] {cell} [2] [$rowIterator],
					row  => $rowIterator,
					data => $Columns
				}
			);

		}

	}

	return;

}


# Subroutine for checking length of (potentially un-initialized) cells
sub Check_Cell_Length {
	my $target_cell_contents = shift;
	my $target_len_thresh = shift;

	my $target_cell_length = defined ($target_cell_contents) ? length ($target_cell_contents) : 0;

	my $target_cell_exceeds_len_thresh_flag = 0;

	if ( $target_cell_length > $target_len_thresh) { $target_cell_exceeds_len_thresh_flag = 1; }

	return $target_cell_exceeds_len_thresh_flag;
}



return 1;