use strict;
use warnings;

use Spreadsheet::ParseExcel;

print " - ", "Setting file variables... ";
my $file_name = "mission 115";
my $file_ext  = "xls";
my $file_path = "C:\\RTscada\\database\\";
print "done.\n";

my $file_handle = $file_path . $file_name . "." . $file_ext;

my $parser   = Spreadsheet::ParseExcel->new();
my $workbook = $parser->parse($file_handle);

if ( !defined $workbook ) {
        die $parser->error(), ".\n";
}

my $no_WorkSheets = $workbook -> worksheet_count();

my $target_sheet  = $workbook -> worksheet($no_WorkSheets - 10);

my ($row, $col) = (30, 10);

my $target_sheet_name = $target_sheet -> get_name();
my $target_cell       = $target_sheet -> get_cell($row-1, $col-1);

print "Sheet name: $target_sheet_name\n",
      " Row, Col : ($row, $col)\n",
      " Cell data: ";

if (defined ($target_cell)) {
	print $target_cell -> unformatted();
}

else { print "udef"; }

print "\n";